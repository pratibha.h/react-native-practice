import React from "react";
import { View, Button, Text, StyleSheet, /*TouchableOpacity*/ } from "react-native";

const HomeScreen = (props) => {
  // console.log(props)
  return (
    <View>
      <Text style={styles.text}>hey There</Text>
      <Button title="Go to Components Demo" onPress={() => { props.navigation.navigate("Components") }} />
      <Button title="Go to List Demo" onPress={() => { props.navigation.navigate("List") }} />
      <Button title="Go to Image Demo" onPress={() => { props.navigation.navigate("Image") }} />
      <Button title="Go to Counter State Demo" onPress={() => { props.navigation.navigate("Counter") }} />
      <Button title="Go to Color Demo" onPress={() => { props.navigation.navigate("Color") }} />
      <Button title="Go to Square Screen Demo" onPress={() => { props.navigation.navigate("Square") }} />

      {/* <TouchableOpacity style={styles.touchable} onPress={()=>{props.navigation.navigate("List")}}>
    <Text>Go To List Demo</Text>
  </TouchableOpacity> */}
    </View>)
};

const styles = StyleSheet.create({
  text: {
    fontSize: 30,
    textAlign: "center",
    padding: 10

  },
  listButton:
  {
    textAlign: "center",
    padding: 10,
    backgroundColor: "green"
  }

});

export default HomeScreen;
