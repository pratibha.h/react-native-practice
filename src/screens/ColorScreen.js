import React, { useState } from 'react';
import { View, StyleSheet, Button, FlatList, TouchableOpacity } from 'react-native';

const ColorScreen = () => {
    const [colors, setColors] = useState([]);
    console.log(colors);

    return (
        <View >
            <Button
                title="Add a Color"
                onPress={() => {
                    setColors([...colors, randomRgb()]);
                }}
            />
            <View style={{ }}>
                <FlatList
                numColumns="4"
                               
                    keyExtractor={item => item}
                    data={colors}
                    renderItem={({ item }) => {
                        return (
                            <TouchableOpacity onPress={()=>{

                            }}>
                                <View style={{ height: 100, width: 100, backgroundColor: item }} />
                            </TouchableOpacity>
                        );
                    }}
                />
            </View>
        </View>
    );
};

const randomRgb = () => {
    const red = Math.floor(Math.random() * 256);
    const green = Math.floor(Math.random() * 256);
    const blue = Math.floor(Math.random() * 256);

    return `rgb(${red}, ${green}, ${blue})`;
};

const styles = StyleSheet.create({});

export default ColorScreen;