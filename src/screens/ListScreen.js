import React from 'react';
import { View, StyleSheet, Text, FlatList } from 'react-native';
const ListScreen = () => {
    const arrayData = [
        { name: "AGILE" },
        { name: "HTML" },
        { name: "CSS" },
        { name: "GIT" },
        { name: "RDBMS" },
        { name: "JAVASCRIPT" },
        { name: "MONGODB" }
    ]
    return (
        <View>
            <Text style={styles.headingText}>A Demo of List Screen Component Called FlatScreen </Text>
            <Text style={styles.subText}>Here You See a list of things, I have learnt so far </Text>
            <FlatList data={arrayData} renderItem={({ item }) => {
                return <Item name={item.name} />
            }} keyExtractor={arrayData => arrayData.name} />
        </View>
    );
};
const Item = (props) => {
    return <Text style={styles.item}> {props.name}</Text>;
}
const styles = StyleSheet.create(
    {
        headingText:
        {
            fontSize: 23,
            color: "red",
            textAlign: "center",
            padding: 10

        },
        subText:
        {
            fontSize: 18,
            color: "green",
            textAlign: "center",
            padding: 10
        },
        item:
        {
            color: "blue",
            fontSize: 18,
            textAlign: "justify",
            padding: 2,
            marginVertical: 1,
            marginHorizontal: 150,
            borderWidth: 1,
            borderColor: '#d6d7da',
            borderRadius: 8,


        }

    });
export default ListScreen