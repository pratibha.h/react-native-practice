import React, { useState } from 'react'
import { StyleSheet, Text, View, Button } from 'react-native'
const SquareScreen = () => {
    const [red, setRed] = useState(0);
    const [green, setGreen] = useState(0);
    const [blue, setBlue] = useState(0);


}
const ViewScreen = (props) => {
    return (
        <View>
            <Text style={styles.colorText}>{props.color}</Text>
            <Button title={`More  ${props.color}`} />
            <Button title={`Less  ${props.color}`} />
        </View>
    )
}

const CallingScreen = (props) => {
    return (
        <View>
            <ViewScreen color="red" />
            <ViewScreen color="green" />
            <ViewScreen color="blue" />
        </View>

    )
}
const styles = StyleSheet.create(
    {
        colorText:{
            fontSize:25,
            padding:12,
            fontWeight:"bold",
            // fontFamily:'lucida grande', "tahoma", "verdana", "arial", "sans-serif"
    }
}
)
export default CallingScreen