import React,{useState} from "react";
import { Text, StyleSheet, View, Button } from 'react-native';

const CounterScreen = () => {
   const [count,setCount]=useState(0);
    return (
        <View>
            <Text style={styles.textStyle}>Current Count: {count}</Text>
            <Button title="Increment" onPress={()=>{setCount(count+1)}}/>
            <Button title="Decrement" onPress={()=>setCount(count-1)}/>
        </View>
    )

}
const styles = StyleSheet.create(
    {
        textStyle: {
            fontSize: 25,
            textAlign:"center",
            padding:10
        },
       
    }
)
export default CounterScreen