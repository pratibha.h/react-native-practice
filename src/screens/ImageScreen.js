import React from "react";
import { Text, StyleSheet, View } from 'react-native';
import ImageDetail from "../Components/ImageDetails"
export const ImageScreen = () => {
    return (
        <View>
            <ImageDetail title="Forest" image={require("../Images/forest.jpg")} />
            <ImageDetail title="Beach" image={require("../Images/beach.jpg")} />
            <ImageDetail title="Mountain" image={require("../Images/mountain.jpg")} />

        </View>
    )

}
const styles = StyleSheet.create({

})