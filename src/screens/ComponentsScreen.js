import React from "react";
import { Text, StyleSheet, View } from 'react-native';

const ComponentScreen = () => {
    const name = "Pratibha"
    return (
        <View>
            <Text style={styles.textStyle}>This is the demo of View and Text</Text>
            <Text style={styles.subHeaderStyle}>By {name}</Text>
        </View>
    )

}
const styles = StyleSheet.create(
    {
        textStyle: {
            fontSize: 25
        },
        subHeaderStyle:
        {

            fontSize: 50
        }
    }
)
export default ComponentScreen