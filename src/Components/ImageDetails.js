import React from "react";
import {Text,StyleSheet,View, Image} from 'react-native'; 
export const ImageDetail=(props)=>{
    let image={
        uri:props.image
    }
    return(
        <View>
    <Text>{props.title} </Text>
    <Image source={props.image}/>
    </View>
    )
}
const styles=StyleSheet.create({

})
export default ImageDetail